const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.get("/", (req, res) => {
    try {
        res.send("This is testing api");
    } catch (error) {
        console.error(error);
    }
});

app.get("/:name", (req, res) => {
    try {
        const name = req.params.name;
        res.send(`Welcome ${name} to the application`);
    } catch (error) {
        console.error(error);
    }
});

app.get("/form/response", (req, res) => {
    try {
        const form = req.body;
        const jsonData = form.toString();
        res.send(jsonData)
    } catch (error) {
        console.error(error);
    }
});

const port = process.env.PORT | 3000;
app.listen(port, () => {
    console.log('           +-+-+-+-+-+-+-+-+-+-+-+-+')
    console.log('           |  Sample Application   |');
    console.log('           +-+-+-+-+-+-+-+-+-+-+-+-+')
    console.log(`Server is running in port ${port}`);
});
